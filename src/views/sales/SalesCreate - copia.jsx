/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { salesActions, productActions } from '../../actions';
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import { Col, Row, Button, Form, FormGroup, Label, Container, Alert, Table, Input  } from 'reactstrap';
import { useForm, Controller  } from "react-hook-form";
import { history } from '../../helpers';
import NumberFormat from 'react-number-format';

const formatter = new Intl.NumberFormat('es-Es', {
    minimumFractionDigits: 2
})

function SalesCreatePage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
    });
      
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

    //Alertas
    const alert = useSelector(state => state.alert);
    //Mostrar alertas
    const [visible, setVisible] = useState(true);
    const onDismiss = () => setVisible(false);
    
    useEffect(() => {
        if(alert.message){
            setVisible(true); 
            window.setTimeout(()=>{setVisible(false)},5000);   
        }
    },[alert]);

    //Form Tabla
    const { handleSubmit, register, errors, reset, control } = useForm();
    //Form resgistrar venta
    const { handleSubmit:handleSubmitSale, register: registerSale , errors: errorsSale, reset:resetSale, control:controlSale  } = useForm();

    //State de guardado
    const registering = useSelector(state => state.sales.registering);

    //obtener productos para select
    const getting = useSelector(state => state.products.getting);
    const products = useSelector(state => state.products);

    useEffect(() => {
        dispatch(productActions.listProducts());
    },[]);

    const [listProducts, setListProducts] = useState(null);

    useEffect(() => {
        if(products.obtained){
            setListProducts(products.list);
        }
    },[products.obtained]);

    //Tabla de productos añadidos
    const [tableSale, setTableSale] = useState([]);
    const [total, setTotal] = useState(0);

    //Añadir producto a tabla
    const onCreateData = (data, e) => {

        //formatear producto
        data.product = JSON.parse(data.product);
        //Sacar subtotal
        data.total = parseFloat(data.kg) * parseFloat(data.product.price);
        //Añadir al array
        let preSale = tableSale;
        preSale.push(data);
        setTableSale(preSale);
        
        preSale.map((product) => {
            let sum = parseFloat(total)+product.total;
            setTotal(sum);
        })

        //resetear form
        reset({
            product:'',
            kg:''
        });
    };

    //Registrar venta
    const onRegisterSale = (data, e) => {

        if(total == 0 || tableSale.length == 0){
            return;
        }

        data.user = user.id;
        data.agency = user.agency.id;
        data.items = tableSale;
        data.total = total;

        dispatch(salesActions.createSale( data ));
    };

    const removeItem = (name) => {

        let preSale = tableSale.filter(item => item.product.name !== name);
        setTableSale(preSale);
        let sum = 0;
        preSale.map((product) => {
            sum = sum + parseFloat(product.total);
            setTotal(sum);
        })
        if(preSale.length == 0){
            setTotal(0);
        }

    }

    const statusRegister = useSelector(state => state.sales);
    //Verificar si guardo y limpiar form
    useEffect(() => {
        if(statusRegister.success){
            resetSale({ names:'', phone:'', ves:'',dollar:'',eur:'',cop:'',tAmmount:'',tBank:'',tReference:'',pAmmount:'',pBank:'',pReference:''});
            setTotal(0);
            setTableSale([]);
        }
    },[statusRegister.success]);

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
                    <div className="container-fluid">
                        <Container>
                        <Row>
                            <Col sm="12" md={{ size: 8, offset: 2 }}>
                                <h3>Registro de venta</h3>
                                {alert.message &&
                                    <Alert color={`alert ${alert.type}`} isOpen={visible} fade={true}>
                                        <div className="container">
                                            {alert.message}
                                            <button
                                                type="button"
                                                className="close"
                                                aria-label="Close"
                                                onClick={onDismiss}
                                            >
                                                <span aria-hidden="true">
                                                <i className="now-ui-icons ui-1_simple-remove"></i>
                                                </span>
                                            </button>
                                        </div>
                                    </Alert>
                                }
                                <Form onSubmit={handleSubmit(onCreateData)} className="form">
                                    <Row form>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="product">Producto</Label>{' '}
                                                {getting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                                <select className={'form-control' + (errors.product ? ' is-invalid' : '')} name="product"
                                                    ref={register({ 
                                                            required: "El producto es requerido" 
                                                        })}>
                                                        <option key="" name="" value=""></option>
                                                        {listProducts && listProducts.map(list => 
                                                            <option
                                                                key={list.id}
                                                                name={list.name, list.price}
                                                                value={JSON.stringify(list)}
                                                            >
                                                                {`${list.name} - ${formatter.format(list.price)}`}
                                                            </option>
                                                        )}
                                                </select>
                                                {errors.product && <div className="invalid-feedback d-block">{errors.product.message}</div>}
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="kg">Peso</Label>
                                                <Controller
                                                    name="kg"
                                                    control={control}
                                                    rules={{
                                                        min: {
                                                            value: 0.001,
                                                            message: "El peso es requerido"
                                                        },
                                                        required: "El peso es requerido",
                                                    }}
                                                    as={<NumberFormat  className={'form-control' + (errors.kg ? ' is-invalid' : '')} thousandSeparator={true} />}
                                                />
                                                {errors.kg && <div className="invalid-feedback">{errors.kg.message}</div>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="d-flex justify-content-between">
                                        <Button color="primary">
                                            <i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                                        </Button>
                                    </div>
                                </Form>
                                <Table striped responsive>
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Peso</th>
                                            <th>Sub total</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {tableSale && tableSale.map((product, index) => {
                                        return (
                                                <tr key={index}>
                                                    <td>{product.product.name}</td>
                                                    <td>{product.kg}</td>
                                                    <td>{formatter.format(product.total)}</td>
                                                    <td>
                                                        <Button className="btn-link" color="primary" 
                                                            onClick={e => 
                                                                {
                                                                    e.preventDefault(); 
                                                                    removeItem(product.product.name);
                                                                }
                                                            }>
                                                            <i className="fa fa-times-circle"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </Table>
                                <Row xs="12">
                                    <Col><div className="pull-right"><b>Total: {formatter.format(total)}</b> </div></Col>
                                </Row>

                                <Form onSubmit={handleSubmitSale(onRegisterSale)}>
                                    <Row form>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="names">Cliente</Label>
                                                <input
                                                    maxLength="100"
                                                    autoComplete="off"
                                                    className={'form-control' + (errors.names ? ' is-invalid' : '')}
                                                    name="names"
                                                    ref={registerSale({
                                                        required: "El cliente es requerido",
                                                    })}
                                                />
                                                {errorsSale.names && <div className="invalid-feedback d-block">{errorsSale.names.message}</div>}
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="phone">Télefono</Label>
                                                <input
                                                    maxLength="100"
                                                    autoComplete="off"
                                                    ref={registerSale({})}
                                                    className={'form-control' + (errors.phone ? ' is-invalid' : '')}
                                                    name="phone"
                                                />
                                                {errorsSale.phone && <div className="invalid-feedback d-block">{errorsSale.phone.message}</div>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="mb-2"><b>Métodos de pago</b></div>
                                    <div className="form-row">
                                        <FormGroup className="col-md-3">
                                            <Label for="ves">BsF</Label>
                                            <Controller
                                                name="ves"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.ves ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.ves && <div className="invalid-feedback">{errors.ves.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-3">
                                            <Label for="dollar">$ Dólares</Label>
                                            <Controller
                                                name="dollar"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.dollar ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.dollar && <div className="invalid-feedback">{errors.dollar.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-3">
                                            <Label for="eur">€ Euros</Label>
                                            <Controller
                                                name="eur"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.eur ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.eur && <div className="invalid-feedback">{errors.eur.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-3">
                                            <Label for="cop">$ Pesos</Label>
                                            <Controller
                                                name="cop"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.cop ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.cop && <div className="invalid-feedback">{errors.cop.message}</div>}
                                        </FormGroup>
                                    </div>
                                    <div className="mb-2"><b>Transferencia</b></div>
                                    <div className="form-row">
                                        <FormGroup className="col-md-4">
                                            <Label for="tAmmount">Monto</Label>
                                            <Controller
                                                name="tAmmount"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.tAmmount ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.tAmmount && <div className="invalid-feedback">{errors.tAmmount.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-4">
                                            <Label for="tBank">Banco</Label>
                                            <input
                                                maxLength="100"
                                                autoComplete="off"
                                                ref={registerSale({})}
                                                className={'form-control' + (errors.tBank ? ' is-invalid' : '')}
                                                name="tBank"
                                            />
                                            {errorsSale.tBank && <div className="invalid-feedback d-block">{errorsSale.tBank.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-4">
                                            <Label for="tReference">Referencia</Label>
                                            <input
                                                maxLength="100"
                                                autoComplete="off"
                                                ref={registerSale({})}
                                                className={'form-control' + (errors.tReference ? ' is-invalid' : '')}
                                                name="tReference"
                                            />
                                            {errorsSale.tReference && <div className="invalid-feedback d-block">{errorsSale.tReference.message}</div>}
                                        </FormGroup>
                                    </div>
                                    <div className="mb-2"><b>Punto</b></div>
                                    <div className="form-row">
                                        <FormGroup className="col-md-4">
                                            <Label for="pAmmount">Monto</Label>
                                            <Controller
                                                name="pAmmount"
                                                control={controlSale}
                                                as={<NumberFormat  className={'form-control' + (errors.pAmmount ? ' is-invalid' : '')} thousandSeparator={true} />}
                                            />
                                            {errors.pAmmount && <div className="invalid-feedback">{errors.pAmmount.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-4">
                                            <Label for="pBank">Banco</Label>
                                            <input
                                                maxLength="100"
                                                autoComplete="off"
                                                ref={registerSale({})}
                                                className={'form-control' + (errors.pBank ? ' is-invalid' : '')}
                                                name="pBank"
                                            />
                                            {errorsSale.pBank && <div className="invalid-feedback d-block">{errorsSale.pBank.message}</div>}
                                        </FormGroup>
                                        <FormGroup className="col-md-4">
                                            <Label for="pReference">Referencia</Label>
                                            <input
                                                maxLength="100"
                                                autoComplete="off"
                                                ref={registerSale({})}
                                                className={'form-control' + (errors.pReference ? ' is-invalid' : '')}
                                                name="pReference"
                                            />
                                            {errorsSale.pReference && <div className="invalid-feedback d-block">{errorsSale.pReference.message}</div>}
                                        </FormGroup>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <Button color="primary" disabled={registering}>
                                            {registering && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Registrar
                                        </Button>
                                        <Button onClick={e =>{e.preventDefault(); history.goBack();} }>Cancelar</Button>
                                    </div>
                                </Form>
                               
                                    
                            </Col>
                        </Row>
                        </Container>
                    </div>

				</div>
            </div>
        </>
    );
}

export default SalesCreatePage;