/* eslint-disable */
import React, { useEffect, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { inventoryActions } from '../../actions';
import moment from 'moment'
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import DataTable from 'react-data-table-component';
import { InputGroup, InputGroupAddon, Button, Input, Spinner, Row, Col, UncontrolledTooltip, Modal, Table, Form, FormGroup, Label , Alert} from 'reactstrap';
//componente dataTable
import { history } from '../../helpers';
import useDebounce from '../../components/Debounce'; 
import '../../assets/css/table.css';
import NumberFormat from 'react-number-format';
import { useForm, Controller  } from "react-hook-form";

//Componente filtro
const FilterComponent = ({ filterText, onFilter, onClear }) => {
	return  <InputGroup style={{ "width": "200px"}}>
		<Input autoComplete="off" style={{"height": "38px", "marginTop":"10px"}} id="search" type="text" placeholder="buscar" value={filterText} onChange={onFilter} />
		<InputGroupAddon addonType="append">
			<Button onClick={onClear} color="primary"><i className="fa fa-times" aria-hidden="true"></i></Button>
		</InputGroupAddon>
	</InputGroup>
}

function InventoryReadjustmentPage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
  	});
   
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

	const dataInventories = useSelector(state => state.inventories.data);
    const loadingPage = useSelector(state => state.inventories.loading);

	//Verificar data de redux
	useEffect(() => {
		if(dataInventories){
			setData(dataInventories.results);
		}else{

		}
  	},[dataInventories]);
    
	// Inicializar tabla sin data
	const [data, setData] = useState([])

	//Columnas Data table
	const columns = [
		{
			name: 'Sucursal',
			selector: 'agency.name',
			sortable: true,
		},
		{
			name: 'Producto',
			selector: 'product.name',
			sortable: true,
		},
		{
			name: 'Cantidad (kg/Unidad)',
			selector: 'kg',
			sortable: true,
			cell : (row)=>{
				return  <NumberFormat value={row.kg?row.kg.toFixed(3):row.kg} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
        },
		{
			name: 'Fecha',
			selector: 'createdDate',
			sortable: true,
			omit: true,//Esconder
			cell : (row)=>{
				return moment(row.createdDate).utc().format("YYYY-MM-DD")
			},
		},
		{
			name: '',
			button: true,
			width:'250px',
			cell: row => {
				if(row.kg < 0){
					return <>
						<i className="fa fa-exclamation-triangle text-warning"></i>&nbsp;Falta registro de entrada
					</>
				}else{
					return <>
						<Button color="primary" size="sm" onClick={e => 
							{
								e.preventDefault(); 
								setModalVisible(true);
								setEditRow(row);
							}
						}>Editar
                    	</Button>
                	</>
				}
               
			} 
		},
	];

	//data inicial
	const getDataTable = () => {
		dispatch(inventoryActions.dataTable());
	}
	
	const [filterText, setFilterText] = useState('');
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
	
	//Retraso 500ms input search
	const debouncedSearchTerm = useDebounce(filterText, 500);

	//Header search del DataTable
	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
				if(dataInventories && dataInventories.results){
					setData(dataInventories.results);
				}
			}
		};
		return <FilterComponent onFilter={e => setFilterText(e.target.value) } onClear={handleClear} filterText={filterText} />;
	}, [filterText, resetPaginationToggle]);


	//Filtrar con delay 
	useEffect(() => {
		if(dataInventories && dataInventories.results){
			if (debouncedSearchTerm) {
				setData(dataInventories.results.filter(item => ( 
						(item.product &&  item.product.name.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.agency &&  item.agency.name.toLowerCase().includes(filterText.toLowerCase()))
					) 
				));
			}
		}
	},[debouncedSearchTerm]);

	//Consultar al entrar
	useEffect(() => {
		getDataTable();
	}, []);

	//Opciones de paginacion
	const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };

	//Loader de la tabla
	const CustomLoader = () => {
		return <>
			<Spinner type="grow" color="primary" style={{ width: '3rem', height: '3rem' }} />
		</>
	}

	const headers = [
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Código Producto", key: "product.code" },
		{ label: "Producto", key: "product.name" },
		{ label: "Cantidad (kg/Unidades)", key: "kg" }
    ];
    
    //modal
    const [modalVisible, setModalVisible] = useState(false);
    //fila seleccionada
	const [editRow, setEditRow] =  useState(null);
	const [newValue, setNewValue] =  useState(null);
    //Form Data
    const { handleSubmit, errors, reset, control } = useForm();

    //Registrar data
    const onCreateData = (data, e) => {
		if(editRow){
			//id de inventario 
			let id = editRow.id;
			setNewValue(data.kg)
			dispatch(inventoryActions.updateInventoryReadjustment( id, data  ));
		}
    };

	//State de actualizacion
	const updating = useSelector(state => state.inventories.updating);
	const inventories = useSelector(state => state.inventories);

	//Actualizar estado de inventario al cambio de información
	useEffect(() => {
		if(inventories.success){
			//actualizar los kg en el grid
			let newKg = parseFloat(newValue);
			let newData = data.map(inv => {
				if(inv.id == editRow.id)
				   return Object.assign({}, inv, {kg:newKg})
				return inv
			});
			
			setData(newData);
			setNewValue(null);
			reset({
				kg:''
			});
			setModalVisible(false);
			setEditRow(null);
		}
	},[inventories.success]);

	//Alertas
	const alert = useSelector(state => state.alert);
	//Mostrar alertas
	const [visible, setVisible] = useState(true);
	const onDismiss = () => setVisible(false);

	useEffect(() => {
		if(alert.message){
			setVisible(true); 
			window.setTimeout(()=>{setVisible(false)},5000);   
		}

		//Si hay algun error cerrar modal y limpiar valores
		if(alert.type == "alert-danger"){
			setNewValue(null);
			reset({
				kg:''
			});
			setModalVisible(false);
			setEditRow(null);
		}
	},[alert]);

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
					<div className="flex-column flex-md-row p-3">

						<div className="d-flex justify-content-between" style={{padding:"4px 16px 4px 24px"}}>
							<div className="align-self-center">
								<h3 style={{ marginBottom: '0' }}>Modificar inventario físico</h3>
							</div>
							<Button id="add" onClick={ ()=> history.push('/register-inventory') } className="btn-round btn-icon" color="primary">
								<i className="fa fa-plus" />
							</Button>
							<UncontrolledTooltip placement="bottom" target="add" delay={0}>
								Añadir inventario
							</UncontrolledTooltip>
						</div>
						{alert.message &&
							<Alert color={`alert ${alert.type}`} isOpen={visible} fade={true}>
								<div className="container">
									{alert.message}
									<button
										type="button"
										className="close"
										aria-label="Close"
										onClick={onDismiss}
									>
										<span aria-hidden="true">
										<i className="now-ui-icons ui-1_simple-remove"></i>
										</span>
									</button>
								</div>
							</Alert>
						}
						<Row>
							<Col>
							<DataTable
								className="dataTables_wrapper"
								responsive
								highlightOnHover
								striped
								sortIcon={ <i className="fa fa-arrow-down ml-2" aria-hidden="true"></i> }
								title="Invetario"
								progressPending={loadingPage}
								paginationComponentOptions={paginationOptions}
								progressComponent={<CustomLoader />}
								noDataComponent="No hay registros para mostrar"
								noHeader={true}
								columns={columns}
								data={data}
								pagination
								paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
								subHeader
								subHeaderComponent={subHeaderComponentMemo}
								persistTableHead
							/>
							</Col>
						
						</Row>
                        <Modal toggle={() => {setModalVisible(false); setEditRow(null)}} isOpen={modalVisible} backdrop="static" >
                            <div className="modal-header">
                            <h5 className="modal-title" id="examplemodalMsgLabel">
                                Modificar
                            </h5>
                            <button aria-label="Close" className="close" type="button" onClick={() => {setModalVisible(false); setEditRow(null)}} disabled={updating}>
                                <span aria-hidden={true}>×</span>
                            </button>
                            </div>
                            <div className="modal-body">
                                {editRow && <><Table striped responsive>
                                    <thead>
                                        <tr>
                                            <th>Sucursal</th>
                                            <th>Producto</th>
                                            <th>kg/unidades</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr key={1}>
                                            <td>{editRow.agency.name}</td>
                                            <td>{editRow.product.name}</td>
                                            <td><NumberFormat value={ editRow.kg.toFixed(3) } displayType={'text'} thousandSeparator={true} /></td>
                                        </tr>      
                                    </tbody>
                                </Table>
                                <Form onSubmit={handleSubmit(onCreateData)} className="form">
                                    <Row form>
                                        <Col md={12}>
                                            <Label for="kg">Cantidad</Label>
                                            <FormGroup>
                                                <Controller
                                                    name="kg"
                                                    control={control}
                                                    rules={{
                                                        min: {
                                                            value: 0,
                                                            message: "El peso es requerido"
                                                        },
                                                        required: "El peso es requerido",
                                                    }}
                                                    as={<NumberFormat placeholder="Cantidad" className={'form-control' + (errors.kg ? ' is-invalid' : '')} thousandSeparator={true} />}
                                                />
                                                {errors.kg && <div className="invalid-feedback">{errors.kg.message}</div>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="modal-footer">
                                        <Button color="primary" disabled={updating}>
                                            {updating && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Guardar cambios
                                        </Button>
                                        <Button color="secondary" type="button"onClick={() =>  {setModalVisible(false); setEditRow(null)}} disabled={updating}>
                                            Cerrar
                                        </Button>
                                    </div>
                                </Form>
                                </>
                                }
                                {
                                !editRow && <>
                                    <span className="spinner-border spinner-border-sm mr-1"></span>
                                    <div className="modal-footer">
                                        <Button color="secondary" type="button"onClick={() =>  {setModalVisible(false); setEditRow(null)}}>
                                            Cerrar
                                        </Button>
                                    </div>
                                </>
                                } 
                            </div>
                            
                        </Modal>
					</div>
				</div>
            </div>
        </>
    );
}

export default InventoryReadjustmentPage;