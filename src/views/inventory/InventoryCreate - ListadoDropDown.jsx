/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { inventoryActions, productActions, agencyActions } from '../../actions';
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import { Col, Row, Button, Form, FormGroup, Label, Container, Alert, Table  } from 'reactstrap';
import { useForm, Controller  } from "react-hook-form";
import { history } from '../../helpers';
import NumberFormat from 'react-number-format';

const formatter = new Intl.NumberFormat('es-Es', {
    minimumFractionDigits: 2
})

function InventoryCreatePage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
    });
      
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

    //Alertas
    const alert = useSelector(state => state.alert);
    //Mostrar alertas
    const [visible, setVisible] = useState(true);
    const onDismiss = () => setVisible(false);
    
    useEffect(() => {
        if(alert.message){
            setVisible(true); 
            window.setTimeout(()=>{setVisible(false)},5000);   
        }
    },[alert]);

    //Form Data
    const { handleSubmit, register, errors, reset, control } = useForm();

    //Registrar data
    const onCreateData = (data, e) => {
        data.user = user.id;
        data.kg = data.kg.replace(/,/g, '');
        dispatch(inventoryActions.createInventory( data ));
    };

    //State de guardado
    const registering = useSelector(state => state.inventories.registering);

    //obtener productos para select
    const getting = useSelector(state => state.products.getting);
    const products = useSelector(state => state.products);

    useEffect(() => {
        dispatch(productActions.listProducts());
    },[]);

    const [listProducts, setListProducts] = useState(null);

    useEffect(() => {
        if(products.obtained){
            setListProducts(products.list);
        }
    },[products.obtained]);

    const statusRegister = useSelector(state => state.inventories);
    //Verificar si guardo y limpiar form
    useEffect(() => {
        if(statusRegister.success){
            reset({
                agency:'',
                product:'',
                kg:'',
                pieces:'',
            });
        }
    },[statusRegister.success]);


    //obtener sucursales para select
    const gettingAgency = useSelector(state => state.agencies.getting);
    const agencies = useSelector(state => state.agencies);
    useEffect(() => {
        dispatch(agencyActions.listAgencies());
    },[]);

    const [listAgencies, setListAgencies] = useState(null);

    useEffect(() => {
        if(agencies.obtained){
            setListAgencies(agencies.list);
        }
    },[agencies.obtained]);

    //Form resgistrar venta
    const { handleSubmit:handleSubmitProduct, register: registerProduct , errors: errorsProduct, reset:resetProduct, control:controlProduct  } = useForm();
    //Tabla de productos añadidos
    const [tableProduct, setTableProduct] = useState([]);

    //Añadir producto a tabla
    const onCreateDataProduct = (data, e) => {
        //formatear producto
        data.product = JSON.parse(data.product);
        //Añadir al array
        let products = tableProduct;
        products.push(data);
        setTableProduct(products);

        //resetear form
        resetProduct({
            product:'',
            kg:''
        });
    };

    //Quitar producto de lista
    const removeItem = (name) => {
        let products = tableProduct.filter(item => item.product.name !== name);
        setTableProduct(products);
    }

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
                    <div className="container-fluid">
                        <Container>
                        <Row>
                            <Col sm="12" md={{ size: 8, offset: 2 }}>
                                <h3>Añadir Mercancia</h3>
                                {alert.message &&
                                    <Alert color={`alert ${alert.type}`} isOpen={visible} fade={true}>
                                        <div className="container">
                                            {alert.message}
                                            <button
                                                type="button"
                                                className="close"
                                                aria-label="Close"
                                                onClick={onDismiss}
                                            >
                                                <span aria-hidden="true">
                                                <i className="now-ui-icons ui-1_simple-remove"></i>
                                                </span>
                                            </button>
                                        </div>
                                    </Alert>
                                }
                                <Form onSubmit={handleSubmitProduct(onCreateDataProduct)} className="form">
                                    <Row form>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="product">Producto</Label>{' '}
                                                {getting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                                <select className={'form-control' + (errorsProduct.product ? ' is-invalid' : '')} name="product"
                                                    ref={registerProduct({ 
                                                            required: "El producto es requerido" 
                                                        })}>
                                                        <option key="" name="" value=""></option>
                                                        {listProducts && listProducts.map(list => 
                                                            <option
                                                                key={list.id}
                                                                name={list.name, list.price}
                                                                value={JSON.stringify(list)}>
                                                                {`${list.name} - ${formatter.format(list.price)}`}
                                                            </option>
                                                        )}
                                                </select>
                                                {errorsProduct.product && <div className="invalid-feedback d-block">{errorsProduct.product.message}</div>}
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="kg">Peso</Label>
                                                <Controller
                                                    name="kg"
                                                    control={controlProduct}
                                                    rules={{
                                                        min: {
                                                            value: 0.001,
                                                            message: "El peso es requerido"
                                                        },
                                                        required: "El peso es requerido",
                                                    }}
                                                    as={<NumberFormat  className={'form-control' + (errorsProduct.kg ? ' is-invalid' : '')} thousandSeparator={true} />}
                                                />
                                                {errorsProduct.kg && <div className="invalid-feedback">{errorsProduct.kg.message}</div>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="d-flex justify-content-between">
                                        <Button color="primary">
                                            <i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                                        </Button>
                                    </div>
                                </Form>
                                <Table striped responsive>
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Peso</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {tableProduct && tableProduct.map((product, index) => {
                                        return (
                                                <tr key={index}>
                                                    <td>{product.product.name}</td>
                                                    <td>{product.kg}</td>
                                                    <td>
                                                        <Button className="btn-link" color="primary" 
                                                            onClick={e => 
                                                                {
                                                                    e.preventDefault(); 
                                                                    removeItem(product.product.name);
                                                                }
                                                            }>
                                                            <i className="fa fa-times-circle"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </Table>
                                <Form onSubmit={handleSubmit(onCreateData)} className="form">
                                    <FormGroup>
                                        <Label for="agency">Sucursal</Label>{' '}
                                        {gettingAgency && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                        <select className={'form-control' + (errors.agency ? ' is-invalid' : '')} name="agency"
                                            ref={register({ 
                                                    required: "La sucursal es requerida" 
                                                })}>
                                                <option key="" name="" value=""></option>
                                                {listAgencies && listAgencies.map(list => 
                                                    <option
                                                        key={list.id}
                                                        name={list.id}
                                                        value={list.id}>
                                                        {list.name}
                                                    </option>
                                                )}
                                        </select>
                                        {errors.agency && <div className="invalid-feedback d-block">{errors.agency.message}</div>}
                                    </FormGroup>
                                    <Row form>
                                        <Col md={6}>  
                                            <FormGroup>
                                                <Label for="note">Nota</Label>
                                                <input
                                                    maxLength="100"
                                                    autoComplete="off"
                                                    className={'form-control' + (errors.note ? ' is-invalid' : '')}
                                                    name="note"
                                                    ref={register({
                                                        required: "La nota es requerida",
                                                    })}
                                                />
                                                {errors.note && <div className="invalid-feedback">{errors.note.message}</div>}
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>  
                                            <FormGroup>
                                                <Label for="comment">Comentario</Label>
                                                <input
                                                    maxLength="150"
                                                    autoComplete="off"
                                                    className={'form-control'}
                                                    name="comment"
                                                    ref={register}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="d-flex justify-content-between">
                                        <Button color="primary" disabled={registering}>
                                            {registering && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Guardar
                                        </Button>
                                        <Button onClick={e =>{e.preventDefault(); history.goBack();} }>Cancelar</Button>
                                    </div>
                                </Form>
                            </Col>
                        </Row>
                        </Container>
                    </div>

				</div>
            </div>
        </>
    );
}

export default InventoryCreatePage;