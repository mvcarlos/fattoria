/* eslint-disable */
import React, { useEffect, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { salesActions } from '../../actions';
import moment from 'moment';
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import DataTable from 'react-data-table-component';
import { InputGroup, InputGroupAddon, Button, Input, Spinner, Row, Col, ListGroup, ListGroupItem, ListGroupItemText, Modal, Table } from 'reactstrap';
//componente dataTable
import useDebounce from '../../components/Debounce'; 
import '../../assets/css/table.css';
import NumberFormat from 'react-number-format';
import { CSVLink, CSVDownload } from "react-csv";

//Componente filtro
const FilterComponent = ({ filterText, onFilter, onClear }) => {
	return <InputGroup style={{ "width": "200px"}}>
		<Input autoComplete="off" style={{"height": "38px", "marginTop":"10px"}} id="search" type="text" placeholder="buscar" value={filterText} onChange={onFilter} />
		<InputGroupAddon addonType="append">
			<Button onClick={onClear} color="primary"><i className="fa fa-times" aria-hidden="true"></i></Button>
		</InputGroupAddon>
	</InputGroup>	
}

function PaymentMethodsPage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
  	});
   
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

	const dataSales = useSelector(state => state.sales.data);
    const loadingPage = useSelector(state => state.sales.loading);

	//Verificar data de redux
	useEffect(() => {
		if(dataSales){
			setData(dataSales.results);
		}else{

		}
  	},[dataSales]);
    
	// Inicializar tabla sin data
	const [data, setData] = useState([])

	//Columnas Data table
	const columns = [
		{
			name: 'Sucursal',
			selector: 'agency.name',
			sortable: true,
		},
		{
			name: 'Monto Total',
			selector: 'totalAmount',
            sortable: true,
            cell : (row)=>{
                return <NumberFormat value={row.totalAmount? row.totalAmount.toFixed(2):row.totalAmount } displayType={'text'} thousandSeparator={true} />
			},
		},
		{
			name: 'Fecha',
			selector: 'date',
			sortable: true,
			cell : (row)=>{
				return moment(row.date).utc().format("YYYY-MM-DD")
			},
		},
	];

	//data inicial
	const getDataTable = () => {
		dispatch(salesActions.salesPaymentMethods());
	}
	
	const [filterText, setFilterText] = useState('');
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
	
	//Retraso 500ms input search
	const debouncedSearchTerm = useDebounce(filterText, 500);

	//Header search del DataTable
	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
				if(dataSales && dataSales.results){
					setData(dataSales.results);
				}
			}
		};
		return <FilterComponent onFilter={e =>  setFilterText(e.target.value) } onClear={handleClear} filterText={filterText} />;
	}, [filterText, resetPaginationToggle]);


	//Filtrar con delay 
	useEffect(() => {
		if(dataSales && dataSales.results){
			if (debouncedSearchTerm) {
				setData(dataSales.results.filter(item => ( 
						(item.date && moment(item.date).utc().format("YYYY-MM-DD").toLowerCase().includes(filterText.toLowerCase()))
						|| (item.agency &&  item.agency.name.toLowerCase().includes(filterText.toLowerCase()))
					) 
				));
			}
		}
	},[debouncedSearchTerm]);

	//Consultar al entrar
	useEffect(() => {
		getDataTable();
	}, []);

	//Opciones de paginacion
	const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };

	//Loader de la tabla
	const CustomLoader = () => (
		<>
			<Spinner type="grow" color="primary" style={{ width: '3rem', height: '3rem' }} />
		</>
	);

	//Calcular total general cuando cambie la información
	const [loadingTotal, setLoadingTotal] = useState(false);
	const [general, setGeneral] = useState(0);
	useEffect(() => {
		let sumtotal = 0
		if(data && data.length>0){
			setLoadingTotal(true);
			for (let item of data) {
				let subtotal = parseFloat(item.totalAmount)
	 			sumtotal += subtotal;
			}
		}
		setLoadingTotal(false);
		setGeneral(sumtotal);
	}, [data]);

	//Data al expandir una fila
	const ExpandedComponent = ({ data }) => (
		<ListGroup>
			<ListGroupItem>
                <ListGroupItemText>
					<b>Punto de venta: <NumberFormat value={ data.totalPos.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
					{ (data.totalPos && data.totalPos>0) ? <Button className="btn-link" color="primary" onClick={()=>{getDetails(data.date, 5)}}>
						Detalle
					</Button>:'' }
				</ListGroupItemText>
				<ListGroupItemText>
					<b>Efectivo Bs: <NumberFormat value={ data.totalVes.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
				</ListGroupItemText>
                <ListGroupItemText>
					<b>Dólar: <NumberFormat value={ data.totalDollar.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
					{ (data.totalDollar && data.totalDollar>0) ? <Button className="btn-link" color="primary" onClick={()=>{getDetails(data.date, 1)}}>
						Detalle
					</Button>:'' }
				</ListGroupItemText>
                <ListGroupItemText>
					<b>Euros: <NumberFormat value={ data.totalEur.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
					{ (data.totalEur && data.totalEur>0) ? <Button className="btn-link" color="primary" onClick={()=>{getDetails(data.date, 2)}}>
						Detalle
					</Button>:'' }
				</ListGroupItemText>
                <ListGroupItemText>
					<b>Pesos: <NumberFormat value={ data.totalCop.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
					{ (data.totalCop && data.totalCop>0) ? <Button className="btn-link" color="primary" onClick={()=>{getDetails(data.date, 3)}}>
						Detalle
					</Button>:'' }
				</ListGroupItemText>
                <ListGroupItemText>
					<b>Transferencias: <NumberFormat value={ data.totalTransfer.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b>
					{ (data.totalTransfer && data.totalTransfer>0) ? <Button className="btn-link" color="primary" onClick={()=>{getDetails(data.date, 4)}}>
						Detalle
					</Button>:'' }
				</ListGroupItemText>
			</ListGroupItem>
	  	</ListGroup>
	);

	const [listDetail, setListDetail] = useState([]);
	const [totalDetail, setTotalDetail] = useState(0);
	const [type, setType] = useState(0);
    const [modalVisible, setModalVisible] = useState(false);

	//Consultar detalle de monedas por fecha y tipo de moneda
	const getDetails = (date, type) => {
		let data = {
			date,
			coin:type
		}
		setType(type);
		dispatch(salesActions.salesDetailPaymentMethods(data));
		//abrir modal
		setModalVisible(true);
	}

	//State de detalle
	const loadingDetail = useSelector(state => state.sales.loadingDetail);
	const saleDetail = useSelector(state => state.sales);

	//Actualizar estado de inventario al cambio de información
	useEffect(() => {
		if(saleDetail.successDetail){
			setTotalDetail(saleDetail.dataDetail.total);
			setListDetail(saleDetail.dataDetail.results);
		}
	},[saleDetail.successDetail]);

	//Header datatable excel
    const headers = [
        { label: "Fecha", key: "date" },
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Monto Total", key: "totalAmount" },
        { label: "Punto de venta", key: "totalPos" },
        { label: "Efectivo Bs", key: "totalVes" },
        { label: "Dólar", key: "totalDollar" },
        { label: "Euros", key: "totalEur" },
        { label: "Pesos", key: "totalCop" },
		{ label: "Transferencias", key: "totalTransfer" }
	];

	//Header transferencias excel
	const headersTransfer = [
        { label: "Fecha", key: "createdDate" },
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Ticket", key: "order" },
		{ label: "Monto", key: "tAmmount" },
        { label: "Banco", key: "tBank" },
        { label: "Referencia", key: "tReference" },
	];

	//Header puntos de venta
	const headersPDV = [
        { label: "Fecha", key: "createdDate" },
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Ticket", key: "order" },
		{ label: "Monto", key: "pAmmount" },
		{ label: "Referencia", key: "pReference" },
		{ label: "Terminal", key: "terminal.code" },
		{ label: "Monto Extra", key: "pAmmountExtra" },
		{ label: "Referencia Extra", key: "pReferenceExtra" },
		{ label: "Terminal Extra", key: "terminalExtra.code" },
		{ label: "Sub Total", key: "subTotal" },
	];

	//limpiar data de modal
	const clearModal = () =>{
		setModalVisible(false); 
		setListDetail([]); 
		setTotalDetail(0); 
		setType(0);
	}

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
					<div className="flex-column flex-md-row p-3">

						<div className="d-flex justify-content-between" style={{padding:"4px 16px 4px 24px"}}>
							<div className="align-self-center">
								<h3 style={{ marginBottom: '0' }}>Formas de pago</h3>
							</div>
						</div>
						<Row>
							<Col>
							<DataTable
								className="dataTables_wrapper"
								expandableRows
								expandableRowsComponent={<ExpandedComponent />}
								responsive
								highlightOnHover
								striped
								sortIcon={ <i className="fa fa-arrow-down ml-2" aria-hidden="true"></i> }
								title="Invetario"
								progressPending={loadingPage}
								paginationComponentOptions={paginationOptions}
								progressComponent={<CustomLoader />}
								noDataComponent="No hay registros para mostrar"
								noHeader={true}
								columns={columns}
								data={data}
								pagination
								paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
								subHeader
								subHeaderComponent={subHeaderComponentMemo}
								persistTableHead
							/>
							</Col>
						</Row>
                        {data && data.length > 0 &&
							<CSVLink data={data} separator={";"} headers={headers} filename={"FormasDePago.csv"}>
								Exportar
							</CSVLink>
						}
						<Row xs="12">
							<Col><div className="pull-right">
								{loadingTotal && <span className="spinner-border spinner-border-sm mr-1"></span>} 
								<b>Total: <NumberFormat value={ general ? general.toFixed(2):general } displayType={'text'} thousandSeparator={true} /></b> 
							</div>
							</Col>
						</Row>
						<Modal toggle={() => {clearModal()}} isOpen={modalVisible} className={type == 5 ? "modal-lg":""}>
                            <div className="modal-header">
                            <h5 className="modal-title" id="examplemodalMsgLabel">
                                Detalle
                            </h5>
                            <button
                                aria-label="Close"
                                className="close"
                                type="button"
                                onClick={() =>  {clearModal()}}
                            >
                                <span aria-hidden={true}>×</span>
                            </button>
                            </div>
                            <div className="modal-body">
								{loadingDetail && <span className="spinner-border spinner-border-sm mr-1"></span>}
								{listDetail.length>0 && <><div className="table-wrapper-scroll-y my-custom-scrollbar">
									{/* Tabla de monedas */}
									{(type == 1 || type == 2 || type == 3) && <Table striped responsive>
										<thead>
											<tr>
												<th>Cantidad</th>
												<th>Tasa</th>
												<th>Sub Total</th>
											</tr>
										</thead>
										<tbody>
										{listDetail.length>0 && listDetail.map((detail, index) => {
											return (
												<tr key={index}>
													<td><NumberFormat value={ (type == 1 && detail.dollar) ? (detail.dollar.toFixed(2)) : ((type == 2 && detail.eur) ? (detail.eur.toFixed(2)) : ( (detail.cop) ? detail.cop.toFixed(2):''))} displayType={'text'} thousandSeparator={true} /></td>
													<td><NumberFormat value={ (type == 1 && detail.valueDollar) ? (detail.valueDollar.toFixed(2)) : ((type == 2 && detail.valueEur) ? (detail.valueEur.toFixed(2)) : ((detail.valueCop)?detail.valueCop.toFixed(2):''))} displayType={'text'} thousandSeparator={true} /></td>
													<td><NumberFormat value={ detail.subTotal.toFixed(2) } displayType={'text'} thousandSeparator={true} /></td>
												</tr>
												)
											})
										}     
										</tbody>
                                	</Table>}
									{/* Tabla de transferencias */}
									{type == 4 && <><Table striped responsive>
											<thead>
												<tr>
													<th>Ticket</th>
													<th>Monto</th>
													<th>Referencia</th>
													<th>Banco</th>
												</tr>
											</thead>
											<tbody>
											{listDetail.length>0 && listDetail.map((detail, index) => {
												return (
													<tr key={index}>
														<td>{detail.order}</td>
														<td><NumberFormat value={ detail.tAmmount.toFixed(2) } displayType={'text'} thousandSeparator={true} /></td>
														<td>{detail.tReference}</td>
														<td>{detail.tBank}</td>
													</tr>
													)
												})
											}     
											</tbody>
										</Table>
									</>
									}
									{/* Tabla de puntos de venta */}
									{type == 5 && <><Table striped responsive>
											<thead>
												<tr>
													<th>Ticket</th>
													<th>Monto</th>
													<th>Referencia</th>
													<th>Terminal</th>
													<th>Monto Extra</th>
													<th>Referencia Extra</th>
													<th>Terminal Extra</th>
													<th>Sub Total</th>
												</tr>
											</thead>
											<tbody>
											{listDetail.length>0 && listDetail.map((detail, index) => {
												return (
													<tr key={index}>
														<td>{detail.order}</td>
														<td><NumberFormat value={ detail.pAmmount ? detail.pAmmount.toFixed(2):0 } displayType={'text'} thousandSeparator={true} /></td>
														<td>{detail.pReference ? detail.pReference : ''}</td>
														<td>{detail.terminal ? detail.terminal.code : ''}</td>
														<td><NumberFormat value={ detail.pAmmountExtra ? detail.pAmmountExtra.toFixed(2):0 } displayType={'text'} thousandSeparator={true} /></td>
														<td>{detail.pReferenceExtra ? detail.pReferenceExtra : ''}</td>
														<td>{detail.terminalExtra ? detail.terminalExtra.code: ''}</td>
														<td><NumberFormat value={ detail.subTotal.toFixed(2) } displayType={'text'} thousandSeparator={true} /></td>
													</tr>
													)
												})
											}     
											</tbody>
										</Table>
									</>
									}
								</div>
								{totalDetail > 0 && <Row xs="12">
									{ type==5 && <Col>
										<div className="pull-left"> 
											<CSVLink data={listDetail} separator={";"} headers={headersPDV} filename={"PuntosDeVenta.csv"}>
												Exportar
											</CSVLink>
										</div>
									</Col>}
									{ type==4 && <Col>
										<div className="pull-left">
											<CSVLink data={listDetail} separator={";"} headers={headersTransfer} filename={"Transferencias.csv"}>
												Exportar
											</CSVLink>
										</div>
									</Col>}
									<Col><div className="pull-right"><b>Total: <NumberFormat value={ totalDetail.toFixed(2) } displayType={'text'} thousandSeparator={true} /></b> </div></Col>
								</Row>
								}
								</>
								}
                            </div>
                            <div className="modal-footer">
                            <Button color="secondary" type="button" onClick={() => {clearModal()}}>
                                Cerrar
                            </Button>
                            </div>
                        </Modal>
					</div>
				</div>
            </div>
        </>
    );
}

export default PaymentMethodsPage;