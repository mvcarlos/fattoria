/* eslint-disable */
import React, { useEffect, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { inventoryActions } from '../../actions';
import moment from 'moment';
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import DataTable from 'react-data-table-component';
import { InputGroup, InputGroupAddon, Button, Input, Spinner, Row, Col, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
//componente dataTable
import useDebounce from '../../components/Debounce'; 
import '../../assets/css/table.css';
import NumberFormat from 'react-number-format';
import { CSVLink, CSVDownload } from "react-csv";

//Componente filtro
const FilterComponent = ({ filterText, onFilter, onClear }) => (
	<InputGroup style={{ "width": "200px"}}>
		<Input autoComplete="off" style={{"height": "38px", "marginTop":"10px"}} id="search" type="text" placeholder="buscar" value={filterText} onChange={onFilter} />
		<InputGroupAddon addonType="append">
			<Button onClick={onClear} color="primary"><i className="fa fa-times" aria-hidden="true"></i></Button>
		</InputGroupAddon>
	</InputGroup>	
)

function InventoryReportPlusPage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
  	});
   
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();
	const dataInventories = useSelector(state => state.inventories.data);
    const loadingPage = useSelector(state => state.inventories.loading);

	//Verificar data de redux
	useEffect(() => {
		if(dataInventories){
			setData(dataInventories.results);
		}else{

		}
  	},[dataInventories]);
    
	// Inicializar tabla sin data
	const [data, setData] = useState([])

	//Columnas Data table
	const columns = [
        {
			name: 'Sucursal',
			selector: 'agency.name',
			sortable: true,
		},
		{
			name: 'Cod. Producto',
			selector: 'product.code',
			sortable: true,
		},
		{
			name: 'Producto',
			selector: 'product.name',
			sortable: true,
			wrap:true,
		},
		{
			name: 'Cantidad Inicial',
			selector: 'initial',
            sortable: true,
            cell : (row)=>{
				return  <NumberFormat value={row.initial?row.initial.toFixed(3):row.initial} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
		},
        {
			name: 'Compras',
			selector: 'totalIn',
			sortable: true,
			cell : (row)=>{
				return  <NumberFormat value={row.totalIn?row.totalIn.toFixed(3):row.totalIn} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
        },
        {
			name: 'Ventas',
			selector: 'totalSell',
			sortable: true,
			cell : (row)=>{
				return  <NumberFormat value={row.totalSell?row.totalSell.toFixed(3):row.totalSell} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
        },
       /* {
			name: 'Salidas',
			selector: 'totalOut',
			sortable: true,
			cell : (row)=>{
				return  <NumberFormat value={row.totalOut?row.totalOut.toFixed(3):row.totalOut} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
		},*/
		/*{
			name: 'Ajustes',
			selector: 'totalAdjustment',
			sortable: true,
			cell : (row)=>{
				
				let adjustment = Math.sign(row.totalAdjustment);
				if(adjustment == 1){
					return <>
						<i className="fa fa-arrow-down text-danger"></i>&nbsp;
						<NumberFormat value={row.totalAdjustment ? Math.abs(row.totalAdjustment).toFixed(3): row.totalAdjustment} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
					</>
				}else if(adjustment == -1){
					return <>
						<i className="fa fa-arrow-up text-success"></i>&nbsp;
						<NumberFormat value={row.totalAdjustment ? Math.abs(row.totalAdjustment).toFixed(3): row.totalAdjustment } displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
					</>
				}
				return <NumberFormat value={row.totalAdjustment ? Math.abs(row.totalAdjustment).toFixed(3): 0 } displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			
			},
        },*/
        {
			name: 'Cantidad Final',
			sortable: false,
			selector: 'TotalQuantity',
			cell : (row)=>{
				return  <NumberFormat value={row.TotalQuantity?row.TotalQuantity.toFixed(3):row.TotalQuantity} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
			},
        },
		{
			name: 'Fecha',
			selector: 'date',
			sortable: true,
			cell : (row)=>{
				return moment(row.date).utc().format("YYYY-MM-DD")
			},
		},
	];

	const headers = [
		{ label: "Fecha", key: "date" },
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Código Producto", key: "product.code" },
		{ label: "Producto", key: "product.name" },
		{ label: "Cantidad Inicial", key: "initial" },
		{ label: "Compras", key: "totalIn" },
		{ label: "Ventas", key: "totalSell" },
		{ label: "Cantidad Final", key: "TotalQuantity" },
	];

	//data inicial
	const getDataTable = () => {
		dispatch(inventoryActions.dataTableReportInventoriesPlus());
	}
	
	const [filterText, setFilterText] = useState('');
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
	
	//Retraso 500ms input search
	const debouncedSearchTerm = useDebounce(filterText, 500);

	//Header search del DataTable
	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
				if(dataInventories && dataInventories.results){
					setData(dataInventories.results);
				}
			}
		};
		return <FilterComponent onFilter={e =>  setFilterText(e.target.value) } onClear={handleClear} filterText={filterText} />;
	}, [filterText, resetPaginationToggle]);


	//Filtrar con delay 
	useEffect(() => {
		if(dataInventories && dataInventories.results){
			if (debouncedSearchTerm) {
				setData(dataInventories.results.filter(item => ( 
						(item.date &&  moment(item.date).utc().format("YYYY-MM-DD").toLowerCase().includes(filterText.toLowerCase()))
						|| (item.product &&  item.product.name.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.product &&  item.product.code.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.agency &&  item.agency.name.toLowerCase().includes(filterText.toLowerCase()))
					) 
				));
			}
		}
	},[debouncedSearchTerm]);

	//Consultar al entrar
	useEffect(() => {
		getDataTable();
	}, []);

	//Opciones de paginacion
	const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };

	//Loader de la tabla
	const CustomLoader = () => (
		<>
			<Spinner type="grow" color="primary" style={{ width: '3rem', height: '3rem' }} />
		</>
	);

	//Data al expandir una fila
	const ExpandedComponent = ({ data }) => (
		<ListGroup>
			<ListGroupItem>
				<ListGroupItemHeading>{ data.name }</ListGroupItemHeading>
				<ListGroupItemText>
					{ data.address}
				</ListGroupItemText>
				<ListGroupItemText>
					{ data.schedule}
				</ListGroupItemText>
			</ListGroupItem>
	  	</ListGroup>
	);

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
					<div className="flex-column flex-md-row p-3">

						<div className="d-flex justify-content-between" style={{padding:"4px 16px 4px 24px"}}>
							<div className="align-self-center">
								<h3 style={{ marginBottom: '0' }}>Inventario bruto</h3>
							</div>
						</div>
						<Row>
							<Col>
							<DataTable
								className="dataTables_wrapper"
								//expandableRows
								//expandableRowsComponent={<ExpandedComponent />}
								responsive
								highlightOnHover
								//striped
								sortIcon={ <i className="fa fa-arrow-down ml-2" aria-hidden="true"></i> }
								title="Invetario"
								progressPending={loadingPage}
								paginationComponentOptions={paginationOptions}
								progressComponent={<CustomLoader />}
								noDataComponent="No hay registros para mostrar"
								noHeader={true}
								columns={columns}
								data={data}
								pagination
								paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
								subHeader
								subHeaderComponent={subHeaderComponentMemo}
								persistTableHead
							/>
							</Col>
						</Row>
						{data && data.length > 0 &&
							<CSVLink data={data} separator={";"} headers={headers} filename={"ReporteDeInventariosBruto.csv"}>
								Exportar
							</CSVLink>
						}
					</div>
				</div>
            </div>
        </>
    );
}

export default InventoryReportPlusPage;