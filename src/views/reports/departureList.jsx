/* eslint-disable */
import React, { useEffect, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { departureActions } from '../../actions';
import moment from 'moment';
// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar";
import SideBar from "../../components/SideBar/SideBar"
import DataTable from 'react-data-table-component';
import { InputGroup, InputGroupAddon, Button, Input, Spinner, Row, Col, Table } from 'reactstrap';
//componente dataTable sede
import { history } from '../../helpers';
import useDebounce from '../../components/Debounce'; 
import '../../assets/css/table.css';
import NumberFormat from 'react-number-format';
import { CSVLink, CSVDownload } from "react-csv";

//Componente filtro
const FilterComponent = ({ filterText, onFilter, onClear }) => {
	return <InputGroup style={{ "width": "200px"}}>
	<Input autoComplete="off" style={{"height": "38px", "marginTop":"10px"}} id="search" type="text" placeholder="buscar" value={filterText} onChange={onFilter} />
	<InputGroupAddon addonType="append">
		<Button onClick={onClear} color="primary"><i className="fa fa-times" aria-hidden="true"></i></Button>
	</InputGroupAddon>
	</InputGroup>	
}

function DepartureListPage() {

  	useEffect(() => {
		document.body.classList.add("landing-page");
		document.body.classList.add("sidebar-collapse");
		document.documentElement.classList.remove("nav-open");
		return function cleanup() {
			document.body.classList.remove("landing-page");
			document.body.classList.remove("sidebar-collapse");
		};
  	});
   
	//usuario
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

	const dataDeparture = useSelector(state => state.departure.data);
    const loadingPage = useSelector(state => state.departure.loading);

	//Verificar data de redux
	useEffect(() => {
		if(dataDeparture){
			setData(dataDeparture.results);
		}else{

		}
  	},[dataDeparture]);
    
	// Inicializar tabla sin data
	const [data, setData] = useState([])

	//Columnas Data table
	const columns = [
		{
			name: 'Sucursal',
			selector: 'agency.name',
			sortable: true,
		},
		{
			name: 'Nombres',
			selector: 'names',
			sortable: true,
        },
        {
			name: 'Télefono',
			selector: 'phone',
			sortable: true,
        },
        {
			name: 'Tipo',
			selector: 'typeDescription',
			sortable: true,
        },
        {
			name: 'Comentario',
			selector: 'comment',
			sortable: true,
		},
		// {
		// 	name: 'Total',
		// 	selector: 'total',
		// 	sortable: true,
		// 	cell : (row)=>{
		// 		return <NumberFormat value={row.total?row.total.toFixed(2):row.total} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  />
		// 	},
		// },
		{
			name: 'Fecha de registro',
			selector: 'createdDate',
			sortable: true,
			cell : (row)=>{
				return moment(row.createdDate).utc().format("YYYY-MM-DD");
			},
		},
    ];
	
	//En caso de exportar aqui esta el header del excel
    const headers = [
		{ label: "Fecha", key: "createdDate" },
		{ label: "Sucursal", key: "agency.name" },
		{ label: "Nombres", key: "names" },
        { label: "Teléfono", key: "phone" },
        //{ label: "Productos", key: "products" },
		{ label: "Tipo", key: "typeDescription" },
	];

	//data inicial
	const getDataTable = () => {
		dispatch(departureActions.dataTable());
	}
	
	const [filterText, setFilterText] = useState('');
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
	
	//Retraso 500ms input search
	const debouncedSearchTerm = useDebounce(filterText, 500);

	//Header search del DataTable
	const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
				if(dataDeparture && dataDeparture.results){
					setData(dataDeparture.results);
				}
			}
		};
		return <FilterComponent onFilter={e =>  setFilterText(e.target.value) } onClear={handleClear} filterText={filterText} />;
	}, [filterText, resetPaginationToggle]);


	//Filtrar con delay 
	useEffect(() => {
		if(dataDeparture && dataDeparture.results){
			if (debouncedSearchTerm) {
				setData(dataDeparture.results.filter(item => ( 
						(item.createdDate &&  moment(item.createdDate).utc().format("YYYY-MM-DD").toLowerCase().includes(filterText.toLowerCase()))
						|| (item.names &&  item.names.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.phone &&  item.phone.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.typeDescription &&  item.typeDescription.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.comment &&  item.comment.toLowerCase().includes(filterText.toLowerCase()))
						|| (item.agency.name &&  item.agency.name.toString().toLowerCase().includes(filterText.toLowerCase()))
					) 
				));
			}
		}
	},[debouncedSearchTerm]);

	//Consultar al entrar
	useEffect(() => {
		getDataTable();
	}, []);

	//Calcular total general cuando cambie la información
	const [loadingTotal, setLoadingTotal] = useState(false);
	const [general, setGeneral] = useState(0)
	useEffect(() => {
		// let sumtotal = 0
		// if(data && data.length>0){
		// 	setLoadingTotal(true);
		// 	for (let item of data) {
		// 		sumtotal += item.total;
		// 	}
		// }
		// setLoadingTotal(false);
		// setGeneral(sumtotal);
	}, [data]);

	//Opciones de paginacion
	const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };

	//Loader de la tabla
	const CustomLoader = () => (
		<>
			<Spinner type="grow" color="primary" style={{ width: '3rem', height: '3rem' }} />
		</>
	);

	//Data al expandir una fila
	const ExpandedComponent = ({ data }) => (
		<>
		<Table striped responsive>
			<thead>
				<tr>
					<th>Producto</th>
					{/* <th>Precio</th> */}
					<th>kg/unidades</th>
					{/* <th>Total</th> */}
				</tr>
			</thead>
			<tbody>
			{data.products && data.products.map((product, index) => {
				return (
					<tr key={index}>
						<td>{product.name}</td>
						{/* <td><NumberFormat value={product.price.toFixed(2)} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  /></td> */}
						<td><NumberFormat value={product.kg.toFixed(3)} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  /></td>
						{/* <td><NumberFormat value={product.total.toFixed(2)} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  /></td> */}
					</tr>
					)
				})
			}
			</tbody>
    	</Table>
		</>
	);

    return (
        <>
            <div className="d-flex" id="wrapper">
				<SideBar/>
				<div id="page-content-wrapper">
					<AdminNavbar/>
					<div className="flex-column flex-md-row p-3">

						<div className="d-flex justify-content-between" style={{padding:"4px 16px 4px 24px"}}>
							<div className="align-self-center">
								<h3 style={{ marginBottom: '0' }}>Salidas</h3>
							</div>
							<Button id="add" onClick={()=>history.push('/departure')} className="btn-round btn-icon" color="primary">
								<i className="fa fa-plus" />
							</Button>
						</div>
						<Row>
							<Col>
							<DataTable
								className="dataTables_wrapper"
								responsive
								highlightOnHover
								expandableRows
								expandableRowsComponent={<ExpandedComponent />}
								sortIcon={ <i className="fa fa-arrow-down ml-2" aria-hidden="true"></i> }
								title="Salidas"
								progressPending={loadingPage}
								paginationComponentOptions={paginationOptions}
								progressComponent={<CustomLoader />}
								noDataComponent="No hay registros para mostrar"
								noHeader={true}
								columns={columns}
								data={data}
								pagination
								paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
								subHeader
								subHeaderComponent={subHeaderComponentMemo}
								persistTableHead
							/>
							</Col>
						</Row>
                        {/* {data && data.length > 0 &&
							<CSVLink data={data} separator={";"} headers={headers} filename={"ReporteSalidas.csv"}>
								Exportar
							</CSVLink>
						} */}
						{/* {user.role !== 4 &&
                            <Row xs="12">
                                <Col><div className="pull-right">
                                    {loadingTotal && <span className="spinner-border spinner-border-sm mr-1"></span>} 
                                    <b>Total: <NumberFormat value={general.toFixed(2)} displayType={'text'} thousandSeparator={','} decimalSeparator={'.'}  /></b> 
                                </div>
                                </Col>
                            </Row>
                        } */}
						
					</div>
				</div>
            </div>
        </>
    );
}

export default DepartureListPage;