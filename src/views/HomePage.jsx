/* eslint-disable */
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

// core components
import AdminNavbar from "../components/Navbars/AdminNavbar";
//import DefaultFooter from "../components/Footers/DefaultFooter";
import SideBar from "../components/SideBar/SideBar"

function HomePage() {

    const user = useSelector(state => state.authentication.user);

    useEffect(() => {
        document.body.classList.add("landing-page");
        document.body.classList.add("sidebar-collapse");
        document.documentElement.classList.remove("nav-open");
        return function cleanup() {
            document.body.classList.remove("landing-page");
            document.body.classList.remove("sidebar-collapse");
        };
    });

    return (
        <>
            <div className="d-flex" id="wrapper">

            <SideBar/>

            <div id="page-content-wrapper">

                <AdminNavbar/>
                <div className="container-fluid">
                    <h1>Hola {user.firstName}!</h1>
                    <p>
                        <Link to="/login">Salir</Link>
                    </p>
                </div>
            </div>


            </div>

        </>
    );
}

export default HomePage;