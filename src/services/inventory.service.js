/* eslint-disable */
import { apiUrl } from '../config/config';
import authHeader from '../helpers/auth-header';
import handleResponse from '../helpers/handleResponse';
export const inventoryService = {

    inventoryTable: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/table-inventory`, requestOptions);
        return handleResponse(response);
            
    },

    inventoryReportSales: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/report-sales`, requestOptions);
        return handleResponse(response);
            
    },

    //reporte de inventarios
    inventoryTableReport: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/report-inventories`, requestOptions);
        return handleResponse(response);
    },

    //reporte de inventarios sin salidas
    inventoryTableReportPlus: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/report-inventories-plus`, requestOptions);
        return handleResponse(response);
    },

    //reporte de inventarios diario
    inventoryTableReportDaily: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/report-inventories-daily`, requestOptions);
        return handleResponse(response);
    },

    inventoryTableHistory: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/table-history`, requestOptions);
        return handleResponse(response);
            
    },

    inventoryCreate: async (inventory) => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
            body: JSON.stringify(inventory)
        };
        const response = await fetch(`${apiUrl}/inventory/create-inventory`, requestOptions);
        return handleResponse(response);
    },

    inventoryUpdate: async (id, inventory) => {
        const requestOptions = {
            method: 'PUT',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
            body: JSON.stringify(inventory)
        };
        const response = await fetch(`${apiUrl}/inventory/update-inventory/${id}`, requestOptions);
        await handleResponse(response);
        return inventory;
    },

    inventoryReadjustment: async (id, inventory) => {
        const requestOptions = {
            method: 'PUT',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
            body: JSON.stringify(inventory)
        };
        const response = await fetch(`${apiUrl}/inventory/readjustment-inventory/${id}`, requestOptions);
        await handleResponse(response);
        return inventory;
    },

    inventoryGet: async (id) => {
        const requestOptions = {
            method: 'GET',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/get-inventory/${id}`, requestOptions);
        return await handleResponse(response);
    },

    inventoryList: async () => {
        const requestOptions = {
            method: 'GET',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/inventory/get-inventories`, requestOptions);
        return await handleResponse(response);
    }


}

