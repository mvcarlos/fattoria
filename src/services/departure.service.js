/* eslint-disable */
import { apiUrl } from '../config/config';
import authHeader from '../helpers/auth-header';
import handleResponse from '../helpers/handleResponse';

export const departureService = {

    departureTable: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/departure/table-departure`, requestOptions);
        return handleResponse(response); 
    },

    departureCreate: async (departure) => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
            body: JSON.stringify(departure)
        };
        const response = await fetch(`${apiUrl}/departure/create-departure`, requestOptions);
        return handleResponse(response);
    },

    departureGet: async (id) => {
        const requestOptions = {
            method: 'GET',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/departure/get-departure/${id}`, requestOptions);
        return await handleResponse(response);
    },

    departureList: async () => {
        const requestOptions = {
            method: 'GET',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/departure/get-departures`, requestOptions);
        return await handleResponse(response);
    },

    departureReport: async () => {
        const requestOptions = {
            method: 'POST',
            headers: { ... authHeader(), 'Content-Type': 'application/json' },
        };
        const response = await fetch(`${apiUrl}/departure/report-departures`, requestOptions);
        return await handleResponse(response);
    },

}

