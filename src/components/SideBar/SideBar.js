/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { salesActions } from '../../actions';
// reactstrap components
import {
	Collapse,
} from "reactstrap";
import { useLocation, NavLink  } from "react-router-dom";

import { useIdleTimer } from 'react-idle-timer';

//Menu lateral en admin
function SideBar() {


	/**
	 * Antes de cerrar la ventana o tab si hay ventas por procesar
	 * enviar un alerta 
	 */
	window.addEventListener("beforeunload", (ev) => 
	{  
		let inProcess = localStorage.getItem('SALEPROCESS');
		if(inProcess){
			ev.preventDefault();
			return ev.returnValue = '¡Tiene ventas por procesar pendientes!';
		}
	});

	const dispatch = useDispatch();
	const location = useLocation();
	const user = useSelector(state => state.authentication.user);

	// collapse states and functions
	const [collapses, setCollapses] = useState([]);

	const changeCollapse = collapse => {

		if (collapses.includes(collapse)) {
			setCollapses(collapses.filter(prop => prop !== collapse));
		} else {
			setCollapses([...collapses, collapse]);
		}
	};

	/**
	 * En momentos de inactividad realizar consulta de monedas productos y terminales
	 * - Ejecuta la funcion salesDataFormOffline cada 10 segundos
	 */
    const handleOnIdle = () => {
		//console.log('ultima actividad', getLastActiveTime());
		dispatch(salesActions.salesDataFormOffline( user.agency.id ));
        reset();
    }
    const { getLastActiveTime, reset } = useIdleTimer({
        timeout: 10000,//10 segundos
        onIdle: handleOnIdle,
        debounce: 500
	})
	
	//End timer
    useEffect(() => {
		let page = location.pathname;

		if(page === "/users" ||  page === "/register-user"){
			changeCollapse(1);
		}

		if(page === "/agency" ||  page === "/register-agency"){
			changeCollapse(2);
		}

		if(page === "/product" || page === "/register-product" || page === "/product-history"){
			changeCollapse(3);
		}

		if(page === "/register-inventory" || page === "/readjustment"){
			changeCollapse(4);
		}

		if(page === "/sales" || page === "/register-sale" || page === "/sales-daily" || page === "/offline-sales"){
			changeCollapse(5);
		}

		if(page === "/coin" ||  page === "/register-coin"){
			changeCollapse(6);
		}

		if(page === "/terminal" ||  page === "/register-terminal" ||  page === "/update-terminal"){
			changeCollapse(7);
		}
		if(page === "/inventory-sell" || page === "/inventory" || page === "/inventory-history" || page === "/inventory-report" || page === "/inventory-reweigh"
			|| page === "/payment-methods-report" || page === "/inventory-report-daily" || page === "/departures" || page === "/inventory-report-plus" ){
				changeCollapse(8);
		}
		if(page === "/departure"){
			changeCollapse(9);
		}
	}, [location]);
	
	//ventas offline pendientes
	const pending = useSelector(state => state.pending);
	
	const [pendingSales, setPendingSales] = useState('');

	//Colocar cantidad de pendientes en el menú
	const getPendings = () => {
		if(pending.sales && pending.sales.length>0){
			setPendingSales(`(${pending.sales.length})`);
		}else{
			setPendingSales('')
		}
	};
	useEffect(() => {
		getPendings();
	}, [pending.sales]);
	
	
	return (
		<>
			<div className="bg-light border-right" id="sidebar-wrapper">
				<div className="sidebar-heading"><NavLink to="/home">FATTORIA</NavLink></div>

				<div className="list-group list-group-flush" >
					<div aria-multiselectable={true} id="accordion" role="tablist">
						{user.role == 1 &&  //administrador
							<>
								{/* Usuario */}
								<a aria-expanded={collapses.includes(1)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(1);
									}}
								>
									<i className="fa fa-user" aria-hidden="true"></i> Usuarios
								</a>
								<Collapse isOpen={collapses.includes(1)}>
									<NavLink to="/users" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-user" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
								</Collapse>

								{/* Terminales */}
								<a aria-expanded={collapses.includes(7)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(7);
									}}
								>
									<i className="fa fa-barcode" aria-hidden="true"></i> Terminales
								</a>
								<Collapse isOpen={collapses.includes(7)}>
									<NavLink to="/terminal" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-terminal" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
								</Collapse>
								
								{/* Sucursales */}
								<a aria-expanded={collapses.includes(2)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(2);
									}}
								>
									<i className="fa fa-building" aria-hidden="true"></i> Sucursales
								</a>
								<Collapse isOpen={collapses.includes(2)}>
									<NavLink to="/agency" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-agency" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
								</Collapse>

								{/* Catalogo */}
								<a aria-expanded={collapses.includes(3)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(3);
									}}
								>
									<i className="fa fa-list-alt" aria-hidden="true"></i> Catálogo de productos
								</a>
								<Collapse isOpen={collapses.includes(3)}>
									<NavLink to="/product" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-product" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
									<NavLink to="/product-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial
									</NavLink>
								</Collapse>
								
								{/* Inventario */}
								<a aria-expanded={collapses.includes(4)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(4);
									}}
								>
									<i className="fa fa-cubes" aria-hidden="true"></i> Inventario
								</a>
								<Collapse isOpen={collapses.includes(4)}>
									<NavLink to="/register-inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-left" aria-hidden="true"></i> Inicial/Entrada
									</NavLink>	
									<NavLink to="/readjustment" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-cubes" aria-hidden="true"></i> Inventario físico
									</NavLink>
								</Collapse>

								{/* Ventas */}
								<a aria-expanded={collapses.includes(5)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(5);
									}}
								>
									<i className="fa fa-shopping-basket" aria-hidden="true"></i> Ventas {pendingSales !== ''?pendingSales:'' }
								</a>
								<Collapse isOpen={collapses.includes(5)}>
									<NavLink to="/sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas General
									</NavLink>
									<NavLink to="/sales-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas del día
									</NavLink>
									<NavLink to="/offline-sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plug" aria-hidden="true"></i> Ventas Offline {pendingSales !== ''?pendingSales:'' }
									</NavLink>
									<NavLink to="/register-sale" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-shopping-basket" aria-hidden="true"></i> Registrar venta
									</NavLink>
								</Collapse>

								{/* Monedas */}
								<a aria-expanded={collapses.includes(6)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(6);
									}}
								>
									<i className="fa fa-calculator" aria-hidden="true"></i> Monedas
								</a>
								<Collapse isOpen={collapses.includes(6)}>
									<NavLink to="/coin" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-coin" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
								</Collapse>


								{/* Reportes */}
								<a aria-expanded={collapses.includes(8)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(8);
									}}
								>
									<i className="fa fa-table" aria-hidden="true"></i> Reportes
								</a>
								<Collapse isOpen={collapses.includes(8)}>
									<NavLink to="/inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario actual
									</NavLink>
									<NavLink to="/inventory-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte de inventario
									</NavLink>
									<NavLink to="/inventory-report-plus" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario bruto
									</NavLink>
									<NavLink to="/inventory-report-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte diario
									</NavLink>
									<NavLink to="/inventory-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial de inventario
									</NavLink>
									<NavLink to="/inventory-sell" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Ventas
									</NavLink>
									<NavLink to="/departures" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Salidas
									</NavLink>
									<NavLink to="/payment-methods-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-credit-card" aria-hidden="true"></i> Formas de pago
									</NavLink>
								</Collapse>

								{/* Salidas */}
								<a aria-expanded={collapses.includes(9)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(9);
									}}
								>
									<i className="fa fa-arrow-circle-right"></i> Salidas
								</a>
								<Collapse isOpen={collapses.includes(9)}>
									<NavLink to="/departure" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-asterisk" aria-hidden="true"></i> Registrar
									</NavLink>
								</Collapse>
							</>
						}
						{user.role == 2 && //Supervisor
							<>
								<a href="/profile" className="list-group-item bg-light">Perfil</a>
								{/* Catalogo */}
								<a aria-expanded={collapses.includes(3)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(3);
									}}
								>
									<i className="fa fa-list-alt" aria-hidden="true"></i> Catálogo de productos
								</a>
								<Collapse isOpen={collapses.includes(3)}>
									<NavLink to="/product" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/register-product" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plus-circle" aria-hidden="true"></i> Añadir
									</NavLink>
									<NavLink to="/product-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial
									</NavLink>
								</Collapse>
								
								{/* Inventario */}
								<a aria-expanded={collapses.includes(4)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(4);
									}}
								>
									<i className="fa fa-cubes" aria-hidden="true"></i> Inventario
								</a>
								<Collapse isOpen={collapses.includes(4)}>
									<NavLink to="/register-inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-left" aria-hidden="true"></i> Inicial/Entrada
									</NavLink>
									<NavLink to="/readjustment" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-cubes" aria-hidden="true"></i> Inventario físico
									</NavLink>	
								</Collapse>

								{/* Ventas */}
								<a aria-expanded={collapses.includes(5)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(5);
									}}
								>
									<i className="fa fa-shopping-basket" aria-hidden="true"></i> Ventas
								</a>
								<Collapse isOpen={collapses.includes(5)}>
									<NavLink to="/sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas General
									</NavLink>
									<NavLink to="/sales-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas del día
									</NavLink>
								</Collapse>

								{/* Reportes */}
								<a aria-expanded={collapses.includes(8)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(8);
									}}
								>
									<i className="fa fa-table" aria-hidden="true"></i> Reportes
								</a>
								<Collapse isOpen={collapses.includes(8)}>
									<NavLink to="/inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario actual
									</NavLink>
									<NavLink to="/inventory-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte de inventario
									</NavLink>
									<NavLink to="/inventory-report-plus" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario bruto
									</NavLink>
									<NavLink to="/inventory-report-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte diario
									</NavLink>
									<NavLink to="/inventory-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial de inventario
									</NavLink>
									<NavLink to="/inventory-sell" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Ventas
									</NavLink>
									<NavLink to="/payment-methods-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-credit-card" aria-hidden="true"></i> Formas de pago
									</NavLink>
								</Collapse>

								{/* Salidas */}
								<a aria-expanded={collapses.includes(9)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(9);
									}}
								>
									<i className="fa fa-arrow-circle-right"></i> Salidas
								</a>
								<Collapse isOpen={collapses.includes(9)}>
									<NavLink to="/departure" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-asterisk" aria-hidden="true"></i> Registrar
									</NavLink>
								</Collapse>

							</>
						}
						{user.role == 3 && //Gerente
							<>
								<a href="/profile" className="list-group-item bg-light">Perfil</a>
								{/* Catalogo */}
								<a aria-expanded={collapses.includes(3)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(3);
									}}
								>
									<i className="fa fa-list-alt" aria-hidden="true"></i> Catálogo de productos
								</a>
								<Collapse isOpen={collapses.includes(3)}>
									<NavLink to="/product" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Lista
									</NavLink>
									<NavLink to="/product-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial
									</NavLink>
								</Collapse>
								
								{/* Ventas */}
								<a aria-expanded={collapses.includes(5)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(5);
									}}
								>
									<i className="fa fa-shopping-basket" aria-hidden="true"></i> Ventas
								</a>
								<Collapse isOpen={collapses.includes(5)}>
									<NavLink to="/sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas General
									</NavLink>
									<NavLink to="/sales-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas del día
									</NavLink>
								</Collapse>

								{/* Reportes */}
								<a aria-expanded={collapses.includes(8)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(8);
									}}
								>
									<i className="fa fa-table" aria-hidden="true"></i> Reportes
								</a>
								<Collapse isOpen={collapses.includes(8)}>
									<NavLink to="/inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario actual
									</NavLink>
									<NavLink to="/inventory-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte de inventario
									</NavLink>
									<NavLink to="/inventory-report-plus" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario bruto
									</NavLink>
									<NavLink to="/inventory-report-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Reporte diario
									</NavLink>
									<NavLink to="/inventory-history" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-history" aria-hidden="true"></i> Historial de inventario
									</NavLink>
									<NavLink to="/inventory-sell" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Ventas
									</NavLink>
									<NavLink to="/payment-methods-report" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-credit-card" aria-hidden="true"></i> Formas de pago
									</NavLink>
								</Collapse>
							</>
						}
						{user.role == 4 && //Cajero
							<>
								<a href="/profile" className="list-group-item bg-light">Perfil</a>
								
								{/* Reportes */}
								<a aria-expanded={collapses.includes(8)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(8);
									}}
								>
									<i className="fa fa-table" aria-hidden="true"></i> Reportes
								</a>
								<Collapse isOpen={collapses.includes(8)}>
									<NavLink to="/inventory" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Inventario actual
									</NavLink>
								</Collapse>
								{/* Ventas */}
								<a aria-expanded={collapses.includes(5)} className="list-group-item bg-light menu-title"
									data-parent="#accordion"
									href="#"
									id="collapseOne"
									onClick={e => {
										e.preventDefault();
										changeCollapse(5);
									}}
								>
									<i className="fa fa-shopping-basket" aria-hidden="true"></i> Ventas {pendingSales !== ''?pendingSales:'' }
								</a>
								<Collapse isOpen={collapses.includes(5)}>
									<NavLink to="/sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas General
									</NavLink>
									<NavLink to="/sales-daily" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-list" aria-hidden="true"></i> Ventas del día
									</NavLink>
									<NavLink to="/offline-sales" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-plug" aria-hidden="true"></i> Ventas Offline {pendingSales !== ''?pendingSales:'' }
									</NavLink>
									<NavLink to="/register-sale" activeClassName="item-active" className="list-group-item list-group-item-action bg-light">
										<i className="fa fa-shopping-basket" aria-hidden="true"></i> Registrar venta
									</NavLink>
								</Collapse>
							</>
						}
					</div>	
				</div>
			</div>
		</>
	);
}

export default SideBar;
