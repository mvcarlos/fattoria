/* eslint-disable */
import { dataConstants } from '../constants';

export const dataActions = {
    update
};

//actualizar data de ventas offile: monedas, productos y terminales
function update(data) {
    return { type: dataConstants.UPDATE_DATA, data };
}