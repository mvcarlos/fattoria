/* eslint-disable */
import { inventoryConstants } from '../constants';
import { inventoryService } from '../services';
import { alertActions } from './';

export const inventoryActions = {

    dataTable() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryTable()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    //Historial de salida por venta
    dataTableSell() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryReportSales()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    dataTableReportInventories() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryTableReport()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    dataTableReportInventoriesPlus() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryTableReportPlus()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    dataTableReportInventoriesDaily() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryTableReportDaily()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    dataTableHistory() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryTableHistory()
                .then(
                    inventories => {
                        dispatch(success(inventories))
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_TABLE_REQUEST } }
        function success(inventories) { return { type: inventoryConstants.INVENTORY_TABLE_SUCCESS, inventories } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_TABLE_FAILURE, error } }
    },

    //Registrar inventario
    createInventory(inventory) {
        return dispatch => {
            dispatch(request(inventory));

            inventoryService.inventoryCreate(inventory)
                .then(
                    inventory => { 
                        dispatch(success());
                        dispatch(alertActions.success('¡Se ha registrado el inventario correctamente!'));
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request(inventory) { return { type: inventoryConstants.INVENTORY_CREATE_REQUEST, inventory } }
        function success(inventory) { return { type: inventoryConstants.INVENTORY_CREATE_SUCCESS, inventory } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_CREATE_FAILURE, error } }
    },

    //Obtenr información inventario
    getInventory(id) {
        return dispatch => {
            dispatch(request(id));

            inventoryService.inventoryGet(id)
                .then(
                    inventory => {
                        dispatch(success(inventory));
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request(id) { return { type: inventoryConstants.INVENTORY_GET_REQUEST, id } }
        function success(inventory) { return { type: inventoryConstants.INVENTORY_GET_SUCCESS, inventory } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_GET_FAILURE, error } }
    },

    //Actualizar información inventario
    updateInventory(id, inventory) {
        return dispatch => {
            dispatch(request(inventory));

            inventoryService.inventoryUpdate(id,inventory)
                .then(
                    inventory => {
                        dispatch(success(inventory));
                        dispatch(alertActions.success('Los datos han sido actualizados correctamente'));
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request(id) { return { type: inventoryConstants.INVENTORY_UPDATE_REQUEST, id } }
        function success(inventory) { return { type: inventoryConstants.INVENTORY_UPDATE_SUCCESS, inventory } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_UPDATE_FAILURE, error } }
    },

    //Actualizar información inventario reajuste
    updateInventoryReadjustment(id, inventory) {
        return dispatch => {
            dispatch(request(inventory));

            inventoryService.inventoryReadjustment(id,inventory)
                .then(
                    inventory => {
                        dispatch(success(inventory));
                        dispatch(alertActions.success('Los datos han sido actualizados correctamente'));
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request(id) { return { type: inventoryConstants.INVENTORY_UPDATE_REQUEST, id } }
        function success(inventory) { return { type: inventoryConstants.INVENTORY_UPDATE_SUCCESS, inventory } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_UPDATE_FAILURE, error } }
    },

    //Actualizar información carrera
    listInventories() {
        return dispatch => {
            dispatch(request());

            inventoryService.inventoryList()
                .then(
                    list => {
                        dispatch(success(list));
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(alertActions.error(error.toString()));
                    }
                );
        };

        function request() { return { type: inventoryConstants.INVENTORY_SELECT_REQUEST } }
        function success(list) { return { type: inventoryConstants.INVENTORY_SELECT_SUCCESS, list } }
        function failure(error) { return { type: inventoryConstants.INVENTORY_SELECT_FAILURE, error } }
    }
};
