/* eslint-disable */
import React, { useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { history, Role } from './helpers';
import { alertActions } from './actions';
import { PrivateRoute } from './components';
import HomePage from './views/HomePage';
import LoginPage from './views/LoginPage';
import LandingPage from './views/LandingPage'

import ProfilePage from './views/profile/ProfilePage'
import UsersListPage from './views/users/UsersListPage'
import UserCreatePage from './views/users/UserCreatePage';
import UserUpdatePage from './views/users/UserUpdatePage';

//Sucursal
import AgencyListPage from './views/agency/AgencyList';
import AgencyCreatePage from './views/agency/AgencyCreate';
import AgencyUpdatePage from './views/agency/AgencyUpdate';

//Catalogo
import ProductListPage from './views/product/ProductList';
import ProductCreatePage from './views/product/ProductCreate';
import ProductUpdatePage from './views/product/ProductUpdate';
import ProductListHistoryPage from './views/product/ProductHistory';

//Inventario
import InventoryCreatePage from './views/inventory/InventoryCreate';
import InventoryReweighPage from './views/inventory/InventoryReweigh';
import InventoryReadjustmentPage from './views/inventory/inventoryReadjustment';

//Ventas
import SalesListPage from './views/sales/SalesList';
import SalesListDailyPage from './views/sales/SalesListDaily';
import SalesCreatePage from './views/sales/SalesCreate';
import SalesCreateOfflinePage from './views/sales/SalesCreateOffline';

//Monedas
import CoinListPage from './views/coin/CoinList';
import CoinCreatePage from './views/coin/CoinCreate';
//Terminales
import TerminalListPage from './views/terminal/TerminalList';
import TerminalCreatePage from './views/terminal/TerminalCreate';
import TerminalUpdatePage from './views/terminal/TerminalUpdate';

//reportes
import InventorySellPage from './views/reports/inventorySell';
import InventoryHistoryPage from './views/reports/inventoryHistory';
import InventoryListPage from './views/reports/InventoryList';
import InventoryReportPage from './views/reports/inventoryReport';
import InventoryReportDailyPage from './views/reports/inventoryReportDaily';
import PaymentMethodsPage from './views/reports/PaymentMethods';
import DepartureListPage from './views/reports/departureList';
import InventoryReportPlusPage from './views/reports/inventoryReportPlus';
//Salidas por degustación, autoconsumo o donación
import DeparturePage from './views/departures/departureCreate';

function App() {
    //const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }, []);

    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={LandingPage} />
                <Route path="/login" component={LoginPage} />
                <PrivateRoute path="/profile" component={ProfilePage} />
                <PrivateRoute path="/home" component={HomePage} />

                {/* Usuarios */}
                <PrivateRoute path="/users" roles={[Role.Admin]} component={UsersListPage} />
                <PrivateRoute path="/register-user" roles={[Role.Admin]} component={UserCreatePage} />
                <PrivateRoute path="/update-user" roles={[Role.Admin]} component={UserUpdatePage} />

                {/* Sucursales */}
                <PrivateRoute path="/agency" roles={[Role.Admin]} component={AgencyListPage} />
                <PrivateRoute path="/register-agency" roles={[Role.Admin]} component={AgencyCreatePage} />
                <PrivateRoute path="/update-agency" roles={[Role.Admin]} component={AgencyUpdatePage} />

                {/* Productos */}
                <PrivateRoute path="/product" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={ProductListPage} />
                <PrivateRoute path="/register-product" roles={[Role.Admin, Role.Supervisor]} component={ProductCreatePage} />
                <PrivateRoute path="/update-product" roles={[Role.Admin, Role.Supervisor]} component={ProductUpdatePage} />
                <PrivateRoute path="/product-history" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={ProductListHistoryPage} />

                {/* Inventario */}
                <PrivateRoute path="/inventory" roles={[Role.Admin, Role.Supervisor, Role.Manager, Role.Cashier]} component={InventoryListPage} />
                <PrivateRoute path="/register-inventory" roles={[Role.Admin, Role.Supervisor]} component={InventoryCreatePage} />
                <PrivateRoute path="/inventory-reweigh" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryReweighPage} />
                <PrivateRoute path="/readjustment" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryReadjustmentPage} />

                {/* Ventas */}
                <PrivateRoute path="/sales" component={SalesListPage} />
                <PrivateRoute path="/offline-sales" roles={[ Role.Cashier, Role.Admin]} component={SalesCreateOfflinePage} />
                <PrivateRoute path="/sales-daily" component={SalesListDailyPage} />
                <PrivateRoute path="/register-sale" roles={[ Role.Cashier, Role.Admin]} component={SalesCreatePage} />
                
                {/* Monedas */}
                <PrivateRoute path="/coin" roles={[Role.Admin]} component={CoinListPage} />
                <PrivateRoute path="/register-coin" roles={[ Role.Admin]} component={CoinCreatePage} />

                {/* Terminales */}
                <PrivateRoute path="/terminal" roles={[Role.Admin]} component={TerminalListPage} />
                <PrivateRoute path="/register-terminal" roles={[Role.Admin]} component={TerminalCreatePage} />
                <PrivateRoute path="/update-terminal" roles={[Role.Admin]} component={TerminalUpdatePage} />

                {/* Salidas por degustación, autoconsumo o donación */}
                <PrivateRoute path="/departure" roles={[Role.Admin, Role.Supervisor]} component={DeparturePage} />

                {/* Reportes */}
                <PrivateRoute path="/inventory-sell" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventorySellPage} />
                <PrivateRoute path="/inventory-history" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryHistoryPage} />
                <PrivateRoute path="/inventory-report" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryReportPage} />
                <PrivateRoute path="/inventory-report-daily" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryReportDailyPage} />
                <PrivateRoute path="/payment-methods-report" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={PaymentMethodsPage} />
                <PrivateRoute path="/departures" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={DepartureListPage} />
                <PrivateRoute path="/inventory-report-plus" roles={[Role.Admin, Role.Supervisor, Role.Manager]} component={InventoryReportPlusPage} />
                
                <Redirect from="*" to="/" />
            </Switch>
        </Router>     
    );
}

export default App